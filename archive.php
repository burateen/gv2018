<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gv2018
 */

get_header(); 

?>




  <div id="primary" class="content-area" >
    <main id="main" class="site-main">
      <div class="container">
        <header class="page-header">
          <h1 class="page-title">
            <?php single_cat_title(); ?>
          </h1>
          <?php // the_archive_description( '<div class="archive-description">', '</div>' ); ?>
        </header>
      </div>

      <div class="container">
        <section class="stories">

					<?php
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
              $_count = 0;
               while ( have_posts() ) :
								
								the_post();
								
								$_count++;
								
								get_template_part( 'template-parts/content-stories', get_post_format() );
								
								if ($_count == 2 ) : 

								if ( is_mobile() ) { gv2018_banners( 'banner-300x600-1-mobile' ); } else { gv2018_banners( 'banner-300x600-1-desktop' ); }
        			
								endif; if ($_count == 4 ) : 
									
								if ( is_mobile() ) { gv2018_banners( 'banner-300x600-2-mobile' ); } else { gv2018_banners( 'banner-300x600-2-desktop' ); }

								endif; endwhile;
              
              wp_reset_postdata(); wp_reset_query();
              
              
            ?>
        </section>
      </div>
    </main>
    <!-- #main -->
  </div>
  <!-- #primary -->

  <?php
get_footer();