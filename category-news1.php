<?php
/**
 * The template for displaying category
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gv2018
 */

get_header(); 

?>
  <div class="container">
    <div id="primary" class="content-area">
      <main id="main" class="site-main">
        <header class="page-header">
          <h1 class="page-title">
            <?php single_cat_title(); ?>
          </h1>
          <?php // the_archive_description( '<div class="archive-description">', '</div>' ); ?>
        </header>
        <section class="news-section">
          <ul class="news-list">
            <?php
              $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
              $args = array(
                'cat' => '1',
                'orderby' => 'date',
                'posts_per_page' => 40,
                'cache_results' => true, 
                'paged' => $paged,
                'ignore_sticky_posts' => 0
                );

              $_count = 0;

              $query = new WP_Query( $args );

                while ( $query->have_posts() ) :
                  $query->the_post();
                  $_count++;
                  get_template_part( 'template-parts/content-cat-news', get_post_format() );
                  if ($_count == 10) : 
                    if ( is_mobile() ) { 
                      gv2018_banners( 'newscat-banner-mobile' ); 
                      } else {
                      gv2018_banners( 'newscat-banner-desktop' ); 
                      } 
                  endif; endwhile;
              
              wp_reset_postdata(); 
              
            ?>
          </ul>
        </section>
      </main>
    
    <?php get_sidebar(); ?>

    </div>

  </div>

  <?php get_footer(); ?>