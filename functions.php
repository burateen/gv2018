<?php
/**
 * gv2018 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gv2018
 */

if ( ! function_exists( 'gv2018_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function gv2018_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on gv2018, use a find and replace
		 * to change 'gv2018' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'gv2018', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Top', 'gv2018' ),
		) );
		register_nav_menus( array(
			'menu-mobile' => esc_html__( 'Mobile', 'gv2018' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'gv2018_custom_background_args', array(
			'default-color'          => '',
			'default-image'          => '',
			'default-repeat'         => 'no-repeat',
			'default-position-x'     => 'center',
			'default-position-y'     => 'top',
			'default-size'           => 'auto',
			'default-attachment'     => 'fixed',
			'wp-head-callback'       => '_custom_background_cb',
			'admin-head-callback'    => '',
			'admin-preview-callback' => ''
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		/**
		 * Добавляет поддержку форматов постов
		 */
		add_theme_support( 'post-formats', array(
			'gallery', 'video', 'aside'
		) );

		/**
		 * Подключает файлы css для изменения TimyMCE
		 */
		add_editor_style();

	}
endif;
add_action( 'after_setup_theme', 'gv2018_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gv2018_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gv2018_content_width', 690 );
}
add_action( 'after_setup_theme', 'gv2018_content_width', 0 );


/**
 * Get theme banners
 */
function gv2018_banners( $widget_area ) {
	if ( is_active_sidebar(  $widget_area ) ) {
		dynamic_sidebar(  $widget_area );
	}		
}



/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gv2018_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Сайдбар', 'gv2018' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Добавьте сюда виджетов.', 'gv2018' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Блок 1. Десктоп', 'gv2018' ),
		'id'            => 'top-banner-desktop',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div class="top-banner-desktop widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Блок 1. Мобайл', 'gv2018' ),
		'id'            => 'top-banner-mobile',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div class="top-banner-mobile widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Блок 2. Десктоп', 'gv2018' ),
		'id'            => 'banner-300x600-1-desktop',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div class="banner-300x600 grid-item widget">',
		'after_widget'  => '<div class="adv-label"><a href="/reklama/">Реклама на GorodskieVesti.ru</a></div></div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Блок 2. Мобайл', 'gv2018' ),
		'id'            => 'banner-300x600-1-mobile',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div class="banner-300x600 grid-item widget">',
		'after_widget'  => '<div class="adv-label"><a href="/reklama/">Реклама на GorodskieVesti.ru</a></div></div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Блок 3. Десктоп', 'gv2018' ),
		'id'            => 'banner-300x600-2-desktop',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div class="banner-300x600 grid-item widget">',
		'after_widget'  => '<div class="adv-label"><a href="/reklama/">Реклама на GorodskieVesti.ru</a></div></div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Блок 3. Мобайл', 'gv2018' ),
		'id'            => 'banner-300x600-2-mobile',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div class="banner-300x600 grid-item widget">',
		'after_widget'  => '<div class="adv-label"><a href="/reklama/">Реклама на GorodskieVesti.ru</a></div></div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Блок 4. Десктоп', 'gv2018' ),
		'id'            => 'afterpost-banner-desktop',
		'description'   => 'Вставьте сюда HTML-виджет с кодом рекламной площаки',
		'before_widget' => '<div class="afterpost-banner widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
		register_sidebar( array(
		'name'          => esc_html__( 'Блок 4. Мобайл', 'gv2018' ),
		'id'            => 'afterpost-banner-mobile',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div class="afterpost-banner widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
		register_sidebar( array(
		'name'          => esc_html__( 'Блок 5. Десктоп', 'gv2018' ),
		'id'            => 'newscat-banner-desktop',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div class="newscat-banner-desktop widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
		register_sidebar( array(
		'name'          => esc_html__( 'Блок 5. Мобайл', 'gv2018' ),
		'id'            => 'newscat-banner-mobile',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div class="newscat-banner-mobile widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
		register_sidebar( array(
		'name'          => esc_html__( 'Между новостями и Историями', 'gv2018' ),
		'id'            => 'featured-space',
		'description'   => esc_html__( 'Перетащите сюда виджет Featured Posts by Nelio', 'gv2018' ),
		'before_widget' => '<div id="%1$s" class="featured-space widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Блок 6. Десктоп', 'gv2018' ),
		'id'            => 'bottom-banner-desktop',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div id="%1$s" class="bottom-banner-desktop widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Блок 6. Мобайл', 'gv2018' ),
		'id'            => 'bottom-banner-mobile',
		'description'   => esc_html__( 'Вставьте сюда HTML-виджет с кодом рекламной площаки', 'gv2018' ),
		'before_widget' => '<div id="%1$s" class="bottom-banner-mobile widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'gv2018_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gv2018_scripts() {
	wp_enqueue_style( 'gv2018-fonts', get_template_directory_uri() . '/css/fonts.css', array(), md5_file(get_template_directory() . '/css/fonts.css') );
	wp_enqueue_style( 'gv2018-humburger', get_template_directory_uri() . '/css/hamburgers.min.css', array(), md5_file(get_template_directory() . '/css/hamburgers.min.css') );
	wp_enqueue_style( 'gv2018-style', get_stylesheet_uri(), array(), md5_file(get_stylesheet_uri()) );
	wp_enqueue_style( 'gv2018-mediaqueries', get_template_directory_uri() . '/css/media.css', array(), md5_file(get_template_directory() . '/css/media.css') );
	wp_enqueue_script( 'gv2018-navigation', get_template_directory_uri() . '/js/navigation.js', array(), md5_file(get_template_directory() . '/js/navigation.js'), true );
	wp_enqueue_script( 'gv2018-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), md5_file(get_template_directory() . '/js/skip-link-focus-fix.js'), true );
	// wp_enqueue_script( 'gv2018-resize-sensor', get_template_directory_uri() . '/js/ResizeSensor.min.js', array(), md5_file(get_template_directory() . '/js/ResizeSensor.min.js'), true );
	// wp_enqueue_script( 'gv2018-theia-sticky-sidebar', get_template_directory_uri() . '/js/theia-sticky-sidebar.min.js', array(), md5_file(get_template_directory() . '/js/theia-sticky-sidebar.min.js'), true );
	// wp_enqueue_script( 'gv2018-ias', get_template_directory_uri() . '/js/jquery-ias.min.js', array(), md5_file(get_template_directory() . '/js/theia-sticky-sidebar.min.js'), true );
	wp_enqueue_script( 'gv2018-custom-js', get_template_directory_uri() . '/js/custom.js', array('jquery'), md5_file(get_template_directory() . '/js/custom.js'), true );
}
add_action( 'wp_enqueue_scripts', 'gv2018_scripts' );

if ( is_singular() ) wp_enqueue_script( "comment-reply" );

/**
 * Add metabox to post and page
 */
require_once("inc/add-meta-box.php");
add_action( 'wp_head', 'addMetaBoxes' );
function addMetaBoxes(){
		global $post;
		if ( is_singular() ){ 
			$css = get_post_meta( $post->ID, 'my_custom_css', true );
		}
		if (!empty($css)) {
    echo '<style type="text/css" media="screen">';
		echo $css;
		echo '</style>';
		}
		
}



/**
 * Load YandexZen Widget
 */
require_once("inc/yandex-zen-widget.php");

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Добавить миниатюры записей в список в админке.
 */
require get_template_directory() . '/inc/admin-thumbs.php';

/**
 * Собираем JSON данные из разных источников в одном файле
 */
require get_template_directory() . '/inc/get-json-data.php';

/**
 * Подключаем файл с Шорткодами
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Удаляет создание изображения шириной 768рх medium_large
 */
add_filter( 'intermediate_image_sizes', 'delete_intermediate_image_sizes' );
function delete_intermediate_image_sizes( $sizes ) {
    return array_diff( $sizes, array('medium_large') );
}

/**
 * Удаляет версию, манифест и XML-RPC
 */
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
add_filter( 'xmlrpc_enabled', '__return_false' );

/**
 * Показ социальных кнопок от Яндекса
 */
function gv2018_yandex_share_block() { ?>
	
<script async src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script async src="//yastatic.net/share2/share.js"></script>
<div class="ya-share2" data-services="vkontakte,odnoklassniki,gplus,viber,whatsapp,telegram"></div>
<?php }

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emojis' );

/**
 * Allow Editors access to the Appearance menu
 * https://wordpress.org/plugins/editor-menu-and-widget-access/
 */
function gp_allow_edit_theme_options( $caps ) {
	if( ! empty( $caps[ 'edit_pages' ] ) ) {$caps[ 'edit_theme_options' ] = true;}
	return $caps;
}
add_filter( 'user_has_cap', 'gp_allow_edit_theme_options' );

function gp_new_admin_menu() { $user = new WP_User(get_current_user_id()); if (!empty( $user->roles) && is_array($user->roles)) { foreach ($user->roles as $role) $role = $role; }
  if($role == "editor") { remove_submenu_page( 'themes.php', 'themes.php' );}
}
add_action('admin_init', 'gp_new_admin_menu');

/**
 * Attachment Pages Redirect
 * https://wordpress.org/plugins/attachment-pages-redirect/
 */
function sar_attachment_redirect() {  
		global $post;
		if ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent != 0) ) {
			wp_redirect(get_permalink($post->post_parent), 301); // permanent redirect to post/page where image or document was uploaded
			exit;
		} elseif ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent < 1) ) {   // for some reason it doesnt works checking for 0, so checking lower than 1 instead...
			wp_redirect(site_url(), 302); // temp redirect to home for image or document not associated to any post/page
			exit;       
    }
	}
add_action('template_redirect', 'sar_attachment_redirect',1);

/**
 * Remove comment-reply.min.js from footer
 */
// function comments_clean_header_hook(){ wp_deregister_script( 'comment-reply' ); }
// add_action('init','comments_clean_header_hook');

/**
 * Переименование форматов постов
 */
function rename_post_formats($translation, $text, $context, $domain) {
    $names = array(
      'Standard'  => 'Стандарт',
			'Gallery' => 'Фото',
			'Video' => 'Видео',
			'Aside' => 'Реклама'
    );
    if ($context == 'Post format') {
        $translation = str_replace(array_keys($names), array_values($names), $text);
    }
    return $translation;
}
add_filter('gettext_with_context', 'rename_post_formats', 10, 4);

/**
 * Фильтр для даты 
 */

// для даты создания
function main_get_the_date($the_date, $d, $post) {
  $d = 'd.m.Y';
  $date_now = date_format(date_create("now"), $d);
  $date_yesterday = date_format(date_create("yesterday"), $d);
  $post = get_post($post);
  if ( !$post ) {
    return $the_date;
  }
  $the_date = mysql2date( $d, $post->post_date);
  if ($date_now == $the_date) {
    $the_date = esc_html__( 'сегодня в', 'gv2018' );
  } elseif ($date_yesterday == $the_date) {
    $the_date = esc_html__( 'вчера в', 'gv2018' );
  }	
  return $the_date;
}
add_filter( 'get_the_date', 'main_get_the_date', 10, 3 );

// для даты изменения
function main_get_the_modified_date($the_time, $d) {
  $d = 'd.m.Y';
  $date_now = date_format(date_create("now"), $d);
  $date_yesterday = date_format(date_create("yesterday"), $d);
  $the_time = get_post_modified_time($d, null, null, true);
  if ($date_now == $the_time) {
    $the_time = esc_html__( 'сегодня в', 'gv2018' );
  } elseif ($date_yesterday == $the_time) {
    $the_time = esc_html__( 'вчера в', 'gv2018' );
  }
  return $the_time;
}
add_filter( 'get_the_modified_date', 'main_get_the_modified_date', 10, 2 );



/**
 * Функция preg replace, которая убивает тег p вокруг картинки в посте
 */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>(<img[^>]*>)/', '$1<p>', $content);
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<noscript>)?\s*(<\/noscript>)?\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3\4\5', $content);
	$content = preg_replace('/<p>\s*(<object.*>*.<\/object>)\s*<\/p>/iU', '\1', $content);
	$content = preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
	return $content;
}
add_filter('the_content', 'filter_ptags_on_images');



/**
 * Загрузка скриптов и стилей Contact form 7
 */

add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );


/**
 * Кастомный логотип на странице входа
 */
add_action('login_head', 'my_custom_login_logo');
function my_custom_login_logo(){
	echo '<style type="text/css">
	h1 a { background-image:url('.get_template_directory_uri().'/img/gv_logo_transparent-min.png) !important; }
	</style>';
	}

/**
 * Блок поделиться Яндекса
 */
 function gv2018_yandex_share() { ?>
	<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
	<script src="//yastatic.net/share2/share.js"></script>
	<div class="ya-share2" data-services="vkontakte,odnoklassniki,viber,whatsapp,telegram" data-counter=""></div>
 <?php }


/**
 * Кнопкка "Закрыть материал"
 */
 function gv2018_close_post_button() { ?>
		<div class="top-post-line">
			<div class="container-big">
				<?php gv2018_yandex_share(); ?>
				<?php	the_title( '<div class="top-post-line-title hidden-xs hidden-sm">', '</div>' ); ?>
			</div>
			<a href="#" class="back-button" onclick="window.history.go(-1); return false;"></a>
	</div>

 <?php }

 /**
	* Удаляем type=text/javasrcipt из тегов
  */
add_filter('script_loader_tag', 'clean_script_tag');
function clean_script_tag($input) {
	$input = str_replace("type='text/javascript' ", '', $input);
	return str_replace("'", '"', $input);
	}

/**
 * Replace the 'editor' with a value you need. For example:
 * 'edit_others_posts'. User roles and custom capabilities
 * are allowed.
 */
$su_generator_capability = 'editor';
add_option( 'su_option_generator_access', $su_generator_capability, '', false  );
	
/**
 * Filter hardcoded .wp-caption inline width
 */
// add_filter( 'img_caption_shortcode_width', '__return_zero' );

/**
 * Микроразметка статей для Яндекса
*/

function gv_microdata_for_yandex() {
		$my_tags = get_the_tags();
			$i = 0;
			if ( $my_tags ) {
    	foreach ( $my_tags as $tag ) {
        $tag_names = $tag->name;
    	   echo '<meta itemprop="about" content="' . $tag_names . '"/>';
			 $i++;
			  }
			}

}
?>