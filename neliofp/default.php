<?php

/**
 * You must declare this variable in your templates. It contains the current
 * related post that is about to be displayed.
 */
// global $post;

// Open link A tag that points to the post itself
$open_link = sprintf(
		'<a class="featured-post-title-link" href="%s" title="%s">',
		get_permalink( $post->ID ),
		esc_attr( apply_filters( 'the_title', $post->post_title, $post->ID ) )
	);
$close_link = '</a>';

?>

<article class="post-<?php echo $post->ID; ?> post type-post status-publish entry" itemscope="itemscope" style="background: url( <?php echo get_the_post_thumbnail_url( '', 'medium' ); ?>) no-repeat; background-size: cover;" >

	<?php 
				$post_type = get_post_type();
				if  ($post_type == "post" ) { 
					$category = get_the_category(); $catlink = get_category_link( $category );
					echo '<span class="cat_name '. $category[0]->slug  .'"><a href="'. $category[0]->slug  .'"> '. $category[0]->cat_name . '</a></span>';
				} else {
					false;
				}
	?>
	
	<h2 class="featured-post-title"><?php
		echo $open_link;
		echo apply_filters( 'the_title', $post->post_title, $post->ID );
		echo $close_link;
	?>
	</h2>
	
	<?php if ( !empty( $post->post_excerpt ) ) { echo the_excerpt(); } else {	false; } ?>					

</article>

