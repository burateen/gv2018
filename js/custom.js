jQuery(document).ready(function ($) {

	//Hamburger menu
	$('.hamburger').click(function () {
		$('.mobile-navigation').slideToggle(250);
		$('.hamburger').toggleClass('is-active');
	})


	//Появление-скрытие top-post-line
	$(document).scroll(function () {
		var y = $(this).scrollTop();
		if (y > 100) {
			$('.top-post-line').show();
		} else {
			$('.top-post-line').hide();
		}
	});

	// Обертка iframe для центрирования
	$(".entry-content").children("iframe").wrap("<div class='text-center'></div>");
	$(".instagram-media").wrap("<div class='width690'></div>");


	// ScrollToTop
	var speed = 500,
		$scrollTop = $("<a>")
		.addClass('scrollTop')
		.attr({
			href: '#',
			style: 'display:none; z-index:300; position:fixed;',
			title: 'Наверх'
		})
		.appendTo('body');
	$('a.scrollTop').append('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path fill="#cccccc" d="M19.99 10.424L9.995.089 0 10.424l1.266 1.224 7.728-7.989v16.196h2V3.657l7.731 7.991z"/></svg>');
	$scrollTop.click(function (e) {
		e.preventDefault();
		$('html:not(:animated),body:not(:animated)').animate({
			scrollTop: 0
		}, speed);
	});
	function show_scrollTop() {
		($(window).scrollTop() > 1000) ? $scrollTop.fadeIn(600): $scrollTop.fadeOut(600);
	}
	$(window).scroll(function () {
		show_scrollTop();
	});
	show_scrollTop();


	
})