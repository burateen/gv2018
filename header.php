<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gv2018
 */

?>
	<!doctype html>
	<html <?php language_attributes(); ?>>

	<head>
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<?php if ( is_single() ) { 
			$post_id = get_the_ID(); 
			echo '<meta itemprop="identifier" content="'. $post_id .'">'; 
			echo "\n\t\t";
			echo '<meta property="yandex_recommendations_category" content="'.  wp_get_post_terms(get_the_ID(), 'category')[0]->name .'"/> ';
		} ?>
		
		<link href="http://gmpg.org/xfn/11" rel="profile">
		<link rel="icon" href="https://www.gorodskievesti.ru/favicon.svg" type="image/svg+xml">
		<link rel="mask-icon" href="mask-icon.svg" color="#4169e1">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-icon-180x180.png">
		<link rel="manifest" href="/manifest.webmanifest">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#4169E1">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favicons/ms-icon-144x144.png">
		<meta name="theme-color" content="#4169E1">
		<meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/img/favicons/browserconfig.xml">
		<meta name="apple-mobile-web-app-status-bar-style" content="#4169E1">
		<link rel="preload" href="https://www.gorodskievesti.ru/wp-content/themes/gv2018/css/fonts/oswald-v16-cyrillic_latin-regular.woff2" as="font" type="font/woff2" crossorigin="anonymous">
		<link rel="preload" href="https://www.gorodskievesti.ru/wp-content/themes/gv2018/css/fonts/roboto-v18-cyrillic_latin-regular.woff2" as="font" type="font/woff2" crossorigin="anonymous">
		<link rel="preload" href="https://www.gorodskievesti.ru/wp-content/themes/gv2018/css/fonts/roboto-v18-cyrillic_latin-700.woff2" as="font" type="font/woff2" crossorigin="anonymous">
		<link rel="preload" href="https://www.gorodskievesti.ru/wp-content/themes/gv2018/css/fonts/pt-serif-v9-cyrillic_latin-regular.woff2" as="font" type="font/woff2" crossorigin="anonymous">
		<link rel="preload" href="https://www.gorodskievesti.ru/wp-content/themes/gv2018/css/fonts/pt-serif-v9-cyrillic_latin-700.woff2" as="font" type="font/woff2" crossorigin="anonymous">
		<link rel="preload" href="https://www.gorodskievesti.ru/wp-content/themes/gv2018/css/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0" as="font" type="font/woff2" crossorigin="anonymous">
		<link rel="dns-prefetch preconnect" href="https://r.revda-info.ru">
		<link rel="dns-prefetch preconnect" href="https://yastatic.net">
		<link rel="dns-prefetch preconnect" href="https://an.yandex.ru">
		<link rel="dns-prefetch preconnect" href="https://avatars.mds.yandex.net">
		<link rel="dns-prefetch preconnect" href="https://mc.yandex.ru">
		<link rel="dns-prefetch preconnect" href="https://j.uralweb.ru">
		<link rel="dns-prefetch preconnect" href="https://counter.yadro.ru">
		<link rel="dns-prefetch preconnect" href="https://www.google-analytics.com">

		<?php wp_head(); ?>

		<?php if ( is_paged() ) { ?>
		<meta name="robots" content="noindex,follow" />
		<?php } ?>
		<meta name="robots" content="max-image-preview:large">


		<?php //Начало. Проверка на Режим отладки
			if (WP_DEBUG === false) { 
		?>
		
		<script>window.yaContextCb = window.yaContextCb || []</script>
		<script src="https://yandex.ru/ads/system/context.js" async></script>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-2VPHBQ0TDZ"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'G-2VPHBQ0TDZ');
		</script>

		<!-- Fullscreen banner -->
		<?php	if ( is_mobile() && is_single() ) { ?>
		<!--AdFox START-->
		<!--yandex_gvesti-->
		<!--Площадка: gorodskievesti.ru [mobile] / main / Fullscreen-->
		<!--Категория: <не задана>-->
		<!--Тип баннера: Fullscreen-->
		<div id="adfox_171687749779425864"></div>
		<script>
			window.yaContextCb.push(()=>{
				Ya.adfoxCode.create({
					ownerId: 284466,
					containerId: 'adfox_171687749779425864',
					params: {
						p1: 'cubtw',
						p2: 'hqqh'
					}
				})
			})
		</script>

		<?php } ?>

	<?php //Конец. Проверка на Режим отладки
		} ?>

	</head>

	<body <?php body_class(); ?>>

		<div class="site" id="page">
			<a class="skip-link screen-reader-text" href="#content">
				<?php esc_html_e( 'Перейти к содержимому сайта', 'gv2018' ); ?>
			</a>
			
			<header class="site-header" id="masthead">

				<div class="header-wrap">

					<div class="site-branding">
						<?php if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
								<?php bloginfo( 'name' ); ?>
							</a>
						</h1>
						<?php else : ?>
						<p class="site-title">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
								<?php bloginfo( 'name' ); ?>
							</a>
						</p>
						<?php 	endif;
							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) : ?>
						<p class="site-description">
							<?php echo $description; /* WPCS: xss ok. */ ?>
						</p>
						<?php	endif; ?>

							<div class="site-logo">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<img alt="logo" src="<?php $site_url = get_template_directory_uri(); echo $site_url  . '/img/gv.svg' ?>">
								</a>
							</div>

					</div>

					<nav class="main-navigation" id="site-navigation">
						<?php					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'top-menu',
							) );
						?>
					</nav>

					<div class="post-news">
						<a href="/post-news/">
							<span>
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
							</span>
							<span>
								Сообщить новость
							</span>
						</a>
					</div>


					<div class="search-button">
						<form id="demo-2" method="get" class="search-form" action="https://google.ru/search" target="_blank" >
							<input type="search" class="search-field" placeholder="" value="" name="q" title="Гуглим" />
							<input type="hidden" name="sitesearch" value="www.gorodskievesti.ru" />
						</form>
					</div>

				</div>

				<button class="hamburger hamburger--elastic" type="button" aria-label="Menu">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>

				<div class="mobile-navigation">
					<nav id="mobile-navigation">
						<?php					
						wp_nav_menu( array(
						'theme_location' => 'menu-mobile',
						'menu_id'        => 'menu-mobile',
							) );
					?>
					</nav>

					<div class="mobile-search">
						<form id="search-mobile" method="get" class="mobile-search-form" action="https://google.ru/search">
							<input type="search" class="search-field" placeholder="Поиск" value="" name="qs" title="Гуглим" />
							<input type="hidden" name="sitesearch" value="www.gorodskievesti.ru" />
						</form>
					</div>

					<div class="social-mobile-menu">
						<div class="vk">
							<a href="https://vk.com/gorodskievesti" title="Мы во ВКонтакте">
								<i class="fa fa-vk" aria-hidden="true"></i>
							</a>
						</div>
						<div class="odnoklassniki">
							<a href="https://ok.ru/gorodskievesti" title="Мы в Одноклассниках">
								<i class="fa fa-odnoklassniki" aria-hidden="true"></i>
							</a>
						</div>
						<div class="youtube">
							<a href="//www.youtube.com/channel/UCLaKlDS6DyYcHNdGSeVr7bg" title="Мы на Ютубе">
								<i class="fa fa-youtube" aria-hidden="true"></i>
							</a>
						</div>
					</div>



				</div>
			</header>

			<?php if ( !is_mobile() ) { ?>
			<div class="subheader hidden-xs">
				<div class="container-big">
					<span class="subheader-city">Город
						<a href="//www.gorodskievesti.ru">Первоуральск</a> /
						<a href="//www.revda-info.ru">Ревда</a>
					</span>

					<span class="subheader-weather">
						<a href="/pogoda/" class="hidden-xs">
							Погода в Первоуральске &nbsp;
							<?php 
						$data = gv2018_weather_pervouralsk(); 
						$temp = "{$data->main->temp}";
						$temp = ceil($temp); 
						if ( $temp > 0 ) {
							$temp = "+$temp";
						} else {
							$temp = "$temp";
						}
						echo $temp .'&deg;C';
						$osadki = "{$data->weather[0]->icon}";
						echo '<img src="/wp-content/themes/gv2018/img/weather/' .$osadki. '.svg" alt="alt">';
						?>
						</a>
					</span>

					<span class="subheader-currencies hidden-sm">
						<span class="dollar" title="Курс доллара на сегодня">
							<i class="fa fa-usd"></i>
							<span class="dollar-text">
								<?php $data = CBR_XML_Daily_Ru(); $usd = number_format("{$data->Valute->USD->Value}", 2, '.', ''); echo $usd .' &#x20BD;'; ?>
							</span>
						</span>
						<span class="euro" title="Курс евро на сегодня">
							<i class="fa fa-eur"></i>
							<span class="euro-text">
								<?php $data = CBR_XML_Daily_Ru(); $eur = number_format("{$data->Valute->EUR->Value}", 2, '.', ''); echo $eur .' &#x20BD;';?>
							</span>
						</span>
					</span>

					<span class="subheader-social">
						Присоединяйся &nbsp;
						<a href="//vk.com/gorodskievesti">
							<span class="vk">
								<i class="fa fa-vk"></i>
								<span class="vk-text">
									<?php $data = gv2018_vkontakte_users(); echo "{$data->response[0]->members_count}"; ?>
								</span>
							</span>
						</a>
						<a href="//ok.ru/gorodskievesti">
							<span class="odnoklassniki">
								<i class="fa fa-odnoklassniki"></i>
								<span class="odnoklassniki-text">
									<?php $data = gv2018_odnoklassniki_users(); echo "{$data->counters->members}"; ?>
								</span>
							</span>
						</a>
					</span>
				</div>
			</div>
			<?php } else ?>

			<?php 
			
			
			// $slug = get_page_template_slug();
			// if ($slug !== 'single-hero.php') {

			if ( is_mobile() ) { 
				gv2018_banners( 'top-banner-mobile' ); 
			} 
				else {
					gv2018_banners( 'top-banner-desktop' );
			}
			// }
			?>

<div class="site-content" id="content">