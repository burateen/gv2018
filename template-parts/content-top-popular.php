<?php
/**
 * Template part for displaying news on frontpge
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gv2018
 */

?>


  <div <?php post_class('top-popular'); ?>>
	<h3>Горячие темы <span style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/fire.svg);"></span></h3>
	<?php
		if ( function_exists('wpp_get_mostpopular') ) {
					wpp_get_mostpopular(array(
						'limit' => 7,
						'range' => 'last7days',
						'post_type' => 'post',
						'stats_views' => 0,
						'post_html' => '<li><a href="{url}?utm_source=top_popular">{text_title}</a></li>'
				));
		}
?>
  </div>