<?php
/**
 * Template part for displaying Category News
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gv2018
 */

?>


  <li class="news-list-item">

    <?php if ( has_post_thumbnail() ) { ?>
    <div class="news-list-item-img">
      <a href="<?php echo esc_url (the_permalink()); ?>">
        <?php the_post_thumbnail('thumbnail'); ?>
      </a>
    </div>
    <?php } else { } ?>

    <div class="news-list-item-wrap">
      <div class="meta">
        <span class="meta-time" title="<?php the_time('Y-m-d H:i'); ?>">
          <time class="entry-date published updated" datetime="<?php the_time('Y-m-d');echo "T";the_time('H:i'); ?>">
            <?php echo get_the_date(); echo ' '; echo get_the_time();  ?>
          </time>
        </span>
						<?php if ( in_category( 'novosti-pervouralska' )) { 
							echo ' 
							<span class="novosti-pervouralska">
								<a href="category/novosti-pervouralska/">Первоуральск</a>
							</span>
							'; 
						}	?>
 
      </div>

      <?php

          $format = get_post_format(); 
          switch ($format) {
            case 'gallery':
              echo '<span class="post-format" title="Фото"><i class="fa fa-camera"></i></span>';
              break;
            case 'video':
              echo '<span class="post-format" title="Видео"><i class="fa fa-play-circle"></i></span>';
              break;
            case 'aside':
              echo '<span class="post-format" title="Партнерский материал"><i class="fa fa-handshake-o"></i></span>';
              break;
            
            default:
              
              break;
          }

          the_title( '<h3 class="entry-title news-list-item-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
        ?>
        <div class="excerpt">
          <?php if ( !empty( $post->post_excerpt ) ) { echo the_excerpt(); } else {	false; } ?>
        </div>
    </div>
  </li>