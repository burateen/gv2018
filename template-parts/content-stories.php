<?php
/**
 * Template part for displaying stories on frontpge
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gv2018
 */

?>


  <div class="grid-item">
    
    <div class="grid-item-img">
      <?php if ( has_post_thumbnail() ) { ?>
      <a href="<?php echo the_permalink(); ?>">
        <?php 
            the_post_thumbnail('medium'); 
            
            $format = get_post_format(); 
            switch ($format) {
            case 'gallery':
              echo '<span class="post-format-thumb"><i class="fa fa-camera"></i></span>';
              break;
            case 'video':
              echo '<span class="post-format-thumb"><i class="fa fa-play-circle"></i></span>';
              break;
            case 'aside':
              echo '<span class="post-format-thumb" title="Партнерский материал"><i class="fa fa-handshake-o"></i></span>';
              break;
            default:
              break;
          }
          
          // $category = get_the_category();
          // echo '<span class="cat_name '. $category[0]->slug  .'"> '. $category[0]->cat_name . '</span>';
          ?>

      </a>
      <?php } else { } ?>
    </div>

    <div class="entry-meta">


      <span class="meta-time" title="<?php the_time('Y-m-d H:i'); ?>">
        <time class="entry-date published updated" datetime="<?php the_time('Y-m-d');echo "T";the_time('H:i'); ?>">
          <?php the_time('j F Y'); echo ' года'; ?>
        </time>
      </span>
      <span class="meta-author author hidden">
        <span class="fn">
	        </span>
        <span class="fn">
          <?php the_author_posts_link(); ?>
        </span>
      </span>

					
						<?php if ( in_category( 'novosti-pervouralska' )) { 
							$category_id = get_cat_ID( 'Новости Первоуральска' );
							$category_link = get_category_link( $category_id );
							echo ' 
							<span class="novosti-pervouralska">
								<a href="' . $category_link . '">Первоуральск</a>
							</span>
							'; 
							}	?>
					

    </div>


    <?php the_title( '<h3 class="grid-item-title "><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>

    <?php if ( !empty( $post->post_excerpt ) ) { echo the_excerpt(); } else {	false; } ?>



  </div>