<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gv2018
 */

?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php if ( !empty( $post->post_excerpt ) ) { echo the_excerpt(); } else {	false; } ?>
			<div class="entry-meta">
				<span class="meta-time" title="<?php the_time('Y-m-d H:i'); ?>">
					<time class="entry-date published updated" datetime="<?php the_time('Y-m-d');echo "T";the_time('H:i'); ?>">
						<?php echo get_the_date(); echo ' '; echo get_the_time(); ?>
					</time>
				</span>
				<span class="meta-author author">
					<span class="fn">
					<?php echo get_simple_local_avatar( get_the_author_meta( 'ID' ), 18 ); ?>
					</span>
					<span class="fn">
						<?php the_author_posts_link(); ?>
					</span>
				</span>

	
			</div>
		</header>

		<div class="entry-content">
			<?php
			the_content();
		?>
		
		</div>

		<footer class="entry-footer">
		</footer>
	</article>

<!-- Yandex Native Ads C-A-132712-23 -->
<div id="id-C-A-132712-23"></div>
<script>window.yaContextCb.push(()=>{
  Ya.Context.AdvManager.renderWidget({
    renderTo: 'id-C-A-132712-23',
    blockId: 'C-A-132712-23'
  })
})</script>

	<?php if ( is_mobile() ) { 
				gv2018_banners( 'afterpost-banner-mobile' ); 
				} else {
				gv2018_banners( 'afterpost-banner-desktop' ); 
				} ?>