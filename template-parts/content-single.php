<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gv2018
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
<?php gv_microdata_for_yandex(); ?>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>
		<?php if ( !empty( $post->post_excerpt ) ) { echo the_excerpt(); } else {	false; } ?>
		<div class="entry-meta">
			<span class="meta-time" title="<?php the_time('Y-m-d H:i'); ?>">
				<time class="entry-date published updated" itemprop="datePublished"
					datetime="<?php the_time('Y-m-d');echo "T";the_time('H:i'); ?>">
					<?php echo get_the_date(); echo ' '; echo get_the_time(); ?>
				</time>
			</span>
			<span class="meta-category" itemscope itemtype="http://schema.org/BreadcrumbList">
				<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<?php $category = get_the_category(); 
				echo '<a itemprop="item" class="category" href="'.get_category_link($category[0]->term_id ).'"><span itemprop="name">'
				.$category[0]->cat_name.'</span></a>'; 
				?>
				<meta itemprop="position" content="1" />
				</span>
			</span>
			<span class="meta-author author hidden">
				<span itemprop="author"><?php the_author(); ?></span>
			</span>

		</div>
	</header>

	<div class="entry-content" itemprop="articleBody">
		<?php
			the_content();
		?>

	</div>

	<footer class="entry-footer">
		<div class="views"><?php echo wpp_get_views($post->ID); ?></div>
		<?php the_tags('<ul class="tag-list"><li>#','</li><li>#','</li></ul>'); ?>
	</footer>
</article>

<!-- Yandex Native Ads C-A-132712-23 -->
<div id="id-C-A-132712-23"></div>
<script>window.yaContextCb.push(()=>{
  Ya.Context.AdvManager.renderWidget({
    renderTo: 'id-C-A-132712-23',
    blockId: 'C-A-132712-23'
  })
})</script>

<?php if ( is_mobile() ) { 
				gv2018_banners( 'afterpost-banner-mobile' ); 
				} else {
				gv2018_banners( 'afterpost-banner-desktop' ); 
				} ?>
