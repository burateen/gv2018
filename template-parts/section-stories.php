<?php ?>

<div class="stories-wrap">
	
<section class="stories">

  <?php

  // $currentID = 0;
  // if ( is_single() ) { $currentID = get_the_ID(); }

  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  
  $args = array(
    'orderby' => 'date',
    'posts_per_page' => 11,
    'cache_results' => true, 
    'paged' => $paged,
		'ignore_sticky_posts' => 0,
    // 'post__not_in' => array($currentID) // exclude current post 
    );

  $_count = 0;

  $query = new WP_Query( $args );

    while ( $query->have_posts() ) :
      $query->the_post();
      
      $_count++;
      
      get_template_part( 'template-parts/content-stories', get_post_format() );
      
      if ($_count == 2 ) : 
        
			if ( is_mobile() ) { gv2018_banners( 'banner-300x600-1-mobile' ); } else { gv2018_banners( 'banner-300x600-1-desktop' ); }
			
			endif; if ($_count == 4 ) : 
        
      if ( is_mobile() ) { gv2018_banners( 'banner-300x600-2-mobile' ); } else { gv2018_banners( 'banner-300x600-2-desktop' ); }

      endif; endwhile;

	wp_reset_postdata(); wp_reset_query();


  ?>

</section>
</div>