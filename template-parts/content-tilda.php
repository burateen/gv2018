<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gv2018
 */

?>

		<div class="">
			<?php
			the_content();
		?>
		
		</div>



	<?php if ( is_mobile() ) { 
				gv2018_banners( 'afterpost-banner-mobile' ); 
				} else {
				gv2018_banners( 'afterpost-banner-desktop' ); 
				} ?>

<!-- Yandex Native Ads C-A-132712-23 -->
<div id="id-C-A-132712-23"></div>
<script>window.yaContextCb.push(()=>{
  Ya.Context.AdvManager.renderWidget({
    renderTo: 'id-C-A-132712-23',
    blockId: 'C-A-132712-23'
  })
})</script>