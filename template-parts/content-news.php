<?php
/**
 * Template part for displaying news on frontpge
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gv2018
 */

?>


  <div <?php post_class('top-news-item'); ?>>

    <?php if ( has_post_thumbnail() ) { ?>
    <div class="top-news-item-img">
      <a href="<?php echo esc_url (the_permalink()); ?>">
        <?php the_post_thumbnail('thumbnail'); ?>
      </a>
    </div>
    <?php } else { } ?>

    <div class="top-news-item-wrap">
      <span class="meta-time" title="<?php the_time('Y-m-d H:i'); ?>">
        <time class="entry-date published updated" datetime="<?php the_time('Y-m-d');echo "T";the_time('H:i'); ?>">
          <?php echo get_the_date(); echo ' '; echo get_the_time();  ?>
        </time>
      </span>

      <?php
          $format = get_post_format(); 
          switch ($format) {
            case 'gallery':
              echo '<span class="post-format" title="Фото"><i class="fa fa-camera"></i></span>';
              break;
            case 'video':
              echo '<span class="post-format" title="Видео"><i class="fa fa-play-circle"></i></span>';
              break;
            case 'aside':
              echo '<span class="post-format" title="Партнерский материал"><i class="fa fa-handshake-o"></i></span>';
              break;
            
            default:
              
              break;
          }
          the_title( '<h3 class="entry-title top-news-item-text"><a href="' . esc_url( get_permalink() ) . '" class="top-news-item-link" rel="bookmark">', '</a></h3>' );
        ?>
    </div>
  </div>