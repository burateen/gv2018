<?php
/**
 * Template Name: Большая шапка-картинка
 * Template Post Type: post
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gv2018
 */

get_header(); 

gv2018_close_post_button();

?>
	<div class="entry-header" style="background: url( <?php echo get_the_post_thumbnail_url( '', 'full' ); ?>) no-repeat; background-size: cover;">
		<div class="container">
			<span class="post-cat-title">
				<?php $category = get_the_category(); echo $category[0]->cat_name; ?>
			</span>
			<?php the_title( '<h1 class="entry-title itemprop="headline"">', '</h1>' ); ?>
			<?php if ( !empty( $post->post_excerpt ) ) { echo the_excerpt(); } else {	false; } ?>
		</div>
	</div>


	<div class="container-big">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">

				<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content-single-hero', get_post_type() );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; 
		// End of the loop.
		?>

			</main>
			<?php get_sidebar(); ?>
		</div>
	</div>

	<?php
get_footer();