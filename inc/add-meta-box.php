<?php

// Добавляем блоки в основную колонку на страницах постов и пост. страниц
add_action('add_meta_boxes', 'gv2018_add_custom_box');
function gv2018_add_custom_box(){
	$screens = array( 'post', 'page' );
	add_meta_box( 'gv2018_sectionid', 'Custom CSS', 'gv2018_meta_box_callback', $screens );
}

// HTML код блока
function gv2018_meta_box_callback( $post, $meta ){
	$screens = $meta['args'];

	// Используем nonce для верификации
	wp_nonce_field( plugin_basename(__FILE__), 'gv2018_noncename' );

  // Поля формы для введения данных
  $my_data = get_post_meta( $post->ID, 'my_custom_css', true );
	echo '<p>Введите здесь кастомные стили для поста. Тег &lt;style&gt; писать не нужно.</p>';
	echo '<textarea rows="10" cols="45" id="gv2018_new_field" name="gv2018_new_field">' . $my_data . '</textarea>';
}

// Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'gv2018_save_postdata' );
function gv2018_save_postdata( $post_id ) {
	// Убедимся что поле установлено.
	if ( ! isset( $_POST['gv2018_new_field'] ) )
		return;

	// проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
	if ( ! wp_verify_nonce( $_POST['gv2018_noncename'], plugin_basename(__FILE__) ) )
		return;

	// если это автосохранение ничего не делаем
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
		return;

	// проверяем права юзера
	if( ! current_user_can( 'edit_post', $post_id ) )
		return;

	// Все ОК. Теперь, нужно найти и сохранить данные
	// Очищаем значение поля input.
	$my_data = sanitize_textarea_field( $_POST['gv2018_new_field'] );

	// Обновляем данные в базе данных.
	update_post_meta( $post_id, 'my_custom_css', $my_data );
}
