<?php

function yandex_init_widget()
{
    return register_widget('yandexZenWidget');
}
add_action ('widgets_init', 'yandex_init_widget');

class yandexZenWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'description' => 'Подписка на Яндекс Дзен' );
		$control_ops = array( 'width' => 300 );
		parent::__construct( false, 'Подписка на Яндекс Дзен', $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );
		$title = $instance['title'];

		echo $before_widget;
		echo $before_title; 
		echo $title;
		echo $after_title; 
		?>

		<div class="zen-promo">
			<a class="zen-link" href="https://zen.yandex.ru/gorodskievesti.ru">
				<div class="zen-logo"></div>
				<div class="zen-text">Подпишитесь на наш канал <br>в Яндексе</div>
			</a>
		</div>

		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$params = array( 'title' );
		foreach ( $params as $k ) {
			$instance[$k] = strip_tags( $new_instance[$k] );
		}
        return $instance;
	}

	function form( $instance ) {
		$defaults = array(
			'title' => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		
			}	
	
}
?>