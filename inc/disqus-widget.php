<?php

function disqus_init_widget()
{
    return register_widget('gorodskievestiDisqusCommentsWidget');
}
add_action ('widgets_init', 'disqus_init_widget');

class gorodskievestiDisqusCommentsWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'description' => 'Последние комментарии от Disqus' );
		$control_ops = array( 'width' => 200 );
		parent::__construct( false, '&raquo; Комментарии Disqus', $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );
		$title = $instance['title'];

		echo $before_widget;
		echo $before_title; 
		echo $title;
        echo $after_title; 
		?>
		
		<script type="text/javascript" src="//pervouralsk.disqus.com/combination_widget.js?num_items=5&hide_mods=1&color=grey&default_tab=recent&excerpt_length=7"></script>
		
	    <?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$params = array( 'title' );
		foreach ( $params as $k ) {
			$instance[$k] = strip_tags( $new_instance[$k] );
		}
        return $instance;
	}

	function form( $instance ) {
		$defaults = array(
			'title' => 'Прямой эфир',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
		
		?>
		
			<div class="themespot-widget">
                <table width="100%">
                    <tr>
                        <td class="themespot-widget-label" width="30%"><label for="<?php echo $this->get_field_id('title'); ?>">Заголовок</label></td>
                        <td class="themespot-widget-content" width="70%"><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" /></td>
                    </tr>                   		  
                </table>
            </div>	
		
		<?php
	}	
	
}
?>