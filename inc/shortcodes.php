<?php

/** 
* Telegram 
*/
function gv2018_shortcode_tg() { ob_start(); ?>
<div class="custom-sht tg-sht">
	<div class="custom-sht-img">
		<i class="fa fa-telegram" aria-hidden="true"></i>
	</div>
	<div class="custom-sht-text">
		Хотите читать новости быстро, коротко и весело? 
		<a href="https://t.me/revdainforu" rel="noopener" target="_blank">Подпишитесь</a> 
		на телеграм-канал «Городских вестей»!
	</div>
</div>
<?php return ob_get_clean(); }
add_shortcode('gv-tg', 'gv2018_shortcode_tg');


/** 
* Instagram 
*/
function gv2018_shortcode_ig() { ob_start(); ?>
<div class="custom-sht ig-sht">
	<div class="custom-sht-img">
		<i class="fa fa-instagram" aria-hidden="true"></i>
	</div>
	<div class="custom-sht-text">
		Хотите больше классных фото? Подписывайтесь на 
		<a href="https://www.instagram.com/gorodskievesti/" rel="noopener" target="_blank">Инстаграм «Городских вестей»!</a>
	</div>
</div>
<?php return ob_get_clean(); }
add_shortcode('gv-ig', 'gv2018_shortcode_ig');


/** 
* Vkontakte 
*/
function gv2018_shortcode_vk() { ob_start(); ?>
<div class="custom-sht vk-sht">
	<div class="custom-sht-img">
		<i class="fa fa-vk" aria-hidden="true"></i>
	</div>
	<div class="custom-sht-text">
		Присоединяйтесь к  
		<a href="https://vk.com/gorodskievesti" rel="noopener" target="_blank">«Городским вестям» во «ВКонтакте»!</a>
		Живое общение, открытая стена, фото, видео и котики! 
	</div>
</div>
<?php return ob_get_clean(); }
add_shortcode('gv-vk', 'gv2018_shortcode_vk');


/** 
* Facebook 
*/
function gv2018_shortcode_fb() { ob_start(); ?>
<div class="custom-sht fb-sht">
	<div class="custom-sht-img">
		<i class="fa fa-facebook" aria-hidden="true"></i>
	</div>
	<div class="custom-sht-text">
		Не читаете «ВКонтакте» и «Одноклассники»? Тогда присоединяйтесь к 
		<a href="http://facebook.com/gorodskievesti" rel="noopener" target="_blank">«Городским вестям» в Facebook</a>. 
		Читайте нас там, где вам удобно!
	</div>
</div>
<?php return ob_get_clean(); }
add_shortcode('gv-fb', 'gv2018_shortcode_fb');


/** 
* Odnoklassniki 
*/
function gv2018_shortcode_ok() { ob_start(); ?>
<div class="custom-sht ok-sht">
	<div class="custom-sht-img">
		<i class="fa fa-odnoklassniki" aria-hidden="true"></i>
	</div>
	<div class="custom-sht-text">
		«Городские вести. Добрая компания» — пожалуй, самая доброжелательная группа в «Одноклассниках». 
		<a href="https://ok.ru/gorodskievesti" rel="noopener" target="_blank">Присоединяйтесь</a> 
		к нам, чтобы общаться и читать интересные тексты!
	</div>
</div>
<?php return ob_get_clean(); }
add_shortcode('gv-ok', 'gv2018_shortcode_ok');


/** 
* Native 
*/
function gv2018_shortcode_native() { ob_start(); ?>
<div class="custom-sht native-sht">
	<div class="custom-sht-img">
		<i class="fa fa-line-chart fa-fw" aria-hidden="true"></i>
	</div>
	<div class="custom-sht-text">
		<strong>Это — специальный проект «Городских вестей».</strong> Как мы делаем их, читайте 
		<a href="https://www.gorodskievesti.ru/pro/gvnative/">тут</a>.
	</div>
</div>
<?php return ob_get_clean(); }
add_shortcode('gv-native', 'gv2018_shortcode_native');



/** 
* Native 
*/
function gv2018_shortcode_adv( $atts, $content = null ) { 
	// Attributes
	$atts = shortcode_atts(
		array(
			'bgc' => '#eee',
			'color' => '#000',
			'rekl' => '',
			'url' => '',
			'address' => '',
			'tel' => '',
			'tel1' => '',
		),
		$atts,
		'ri-reklama'
	);
	ob_start(); 

	echo '<div class="custom-sht rekl-sht" style="background-color:'. esc_html( $atts['bgc'] ) .';color:'. esc_html ( $atts['color'] ) .'">';
	echo '<div class="rekl-wrap">';
	echo '<div class="rekl-name"><a href="'. esc_url( $atts['url'] ) .'" rel="noopener" target="_blank">'. esc_html ( $atts['rekl'] ).'</a></div>';
	echo '<div class="rekl-address">Адрес: '. esc_html ( $atts['address'] ) .'.</div>';
	echo '<div class="rekl-tel">Тел: <a href="tel:'. esc_html ( $atts['tel'] ) .'">'. esc_html ( $atts['tel'] ) .'</a></div>';
	if (!empty( $atts['tel1'] )) echo '<div class="rekl-tel">Тел: <a href="tel:'. esc_html ( $atts['tel1'] ) .'">'. esc_html ( $atts['tel1'] ) .'</a></div>';
	echo '</div>';
	echo '<div class="rekl-text">'. esc_html ( $content ) . '</div>';
	echo '<div class="rekl-rekl">Реклама</div>';
	echo '</div>';

return ob_get_clean(); }

add_shortcode('ri-reklama', 'gv2018_shortcode_adv');




?>