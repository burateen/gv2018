<?php

/**
 * Курсы валют https://www.cbr-xml-daily.ru/
 */
function CBR_XML_Daily_Ru() {
    $json_daily_file = __DIR__.'/json/daily_cbr.json';
    if (!is_file($json_daily_file) || filemtime($json_daily_file) < time() - 86400) {
        if ($json_daily = @file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js')) {
            file_put_contents($json_daily_file, $json_daily);
        }
    }
    return json_decode(file_get_contents($json_daily_file));
}
return;

// function CBR_XML_Daily_Ru() {
// 	$response = wp_remote_get( 'https://www.cbr-xml-daily.ru/daily_json.js', array( 
// 		'timeout' => 10, 'httpversion' => '1.1' ) 
// 	);
// if ( ! is_wp_error( $response ) && wp_remote_retrieve_response_code( $response ) === 200 ){
// 	$body = wp_remote_retrieve_body( $response );
// 	$data = json_decode( $body );
// 	update_option( 'gv2018_USD', $data->Valute->USD->Value, false ); 
// 	update_option( 'gv2018_EUR', $data->Valute->EUR->Value, false ); 
// 	}
// }

// if( !wp_next_scheduled('CBR_hook' ) )
// 	wp_schedule_event( time(), 'daily', 'CBR_hook' );

// add_action( 'CBR_hook', 'CBR_XML_Daily_Ru', 10, 0 );

/**
 * Погода в Первоуральске 
 */
function gv2018_weather_pervouralsk() {
	$json_daily_file = __DIR__.'/json/weather.json';
	if (!is_file($json_daily_file) || filemtime($json_daily_file) < time() - 3600) {
			$context = stream_context_create(array('http' => array('timeout' => 3)));
			$json_daily = @file_get_contents('http://api.openweathermap.org/data/2.5/weather?id=510808&appid=4b65f000e53d1a29cc0fd626a503676f&units=metric', 0, $context);
					if (!empty($json_daily)) {
					file_put_contents($json_daily_file, $json_daily);
					}
	}
	return json_decode(file_get_contents($json_daily_file));
}
return;
// function gv2018_weather_pervouralsk() {
// 	$response = wp_remote_get( 'http://api.openweathermap.org/data/2.5/weather?id=510808&appid=4b65f000e53d1a29cc0fd626a503676f&units=metric', array( 
// 		'timeout' => 10, 'httpversion' => '1.1' ) 
// 	);
// if ( ! is_wp_error( $response ) && wp_remote_retrieve_response_code( $response ) === 200 ){
// 	$body = wp_remote_retrieve_body( $response );
// 	$data = json_decode( $body );
// 	update_option( 'gv2018_temp', $data->main->temp, false ); 
// 	update_option( 'gv2018_osadki', $data->weather[0]->icon, false ); 
// 	}
// }

// if( !wp_next_scheduled('weather_hook' ) )
// 	wp_schedule_event( time(), 'hourly', 'weather_hook' );

// add_action( 'weather_hook', 'gv2018_weather_pervouralsk', 10, 0 );

/**
 * Количество пользователей Фейсбуку
 */
// function gv2018_facebook_users() {
//     $json_daily_file = __DIR__.'/json/facebook.json';
//     if (!is_file($json_daily_file) || filemtime($json_daily_file) < time() - 86400) {
//         if ($json_daily = @file_get_contents('https://graph.facebook.com/gorodskievesti?fields=fan_count&access_token=768007079947140|UaW4ko3vnFnU-xuXaoL6OOR6uZE')) {
//             file_put_contents($json_daily_file, $json_daily);
//         }
//     }
//     return json_decode(file_get_contents($json_daily_file));
// }
// return;

// function gv2018_facebook_users() {
// 	$response = wp_remote_get( 'https://graph.facebook.com/gorodskievesti?fields=fan_count&access_token=768007079947140|UaW4ko3vnFnU-xuXaoL6OOR6uZE', array( 
// 		'timeout' => 10, 'httpversion' => '1.1' ) 
// 	);
// if ( ! is_wp_error( $response ) && wp_remote_retrieve_response_code( $response ) === 200 ){
// 	$body = wp_remote_retrieve_body( $response );
// 	$data = json_decode( $body );
// 	update_option( 'gv2018_FB_users', $data->fan_count, false ); 
// 	}
// }

// if( !wp_next_scheduled('FB_hook' ) )
// 	wp_schedule_event( time(), 'daily', 'FB_hook' );

// add_action( 'FB_hook', 'gv2018_facebook_users', 10, 0 );


/**
 * Количество пользователей Вконтакте
 */
function gv2018_vkontakte_users() {
    $json_daily_file = __DIR__.'/json/vkontakte.json';
    if (!is_file($json_daily_file) || filemtime($json_daily_file) < time() - 86400) {
        if ($json_daily = @file_get_contents('https://api.vk.com/method/groups.getById?fields=members_count&v=5.107&group_ids=36223698&access_token=779686fd779686fd779686fd5f77f46fd677796779686fd2d6b1f458659b3b92b3fca0f')) {
            file_put_contents($json_daily_file, $json_daily);
        }
    }
    return json_decode(file_get_contents($json_daily_file));
}
return;

// function gv2018_vkontakte_users() {
// 	$response = wp_remote_get( 'https://api.vk.com/method/groups.getById?fields=members_count&v=5.2&group_ids=36223698&access_token=779686fd779686fd779686fd5f77f46fd677796779686fd2d6b1f458659b3b92b3fca0f', array( 
// 		'timeout' => 10, 'httpversion' => '1.1' ) 
// 	);
// if ( ! is_wp_error( $response ) && wp_remote_retrieve_response_code( $response ) === 200 ){
// 	$body = wp_remote_retrieve_body( $response );
// 	$data = json_decode( $body );
// 	update_option( 'gv2018_VK_users', $data->response[0]->members_count, false ); 
// 	}
// }

// if( !wp_next_scheduled('VK_hook' ) )
// 	wp_schedule_event( time(), 'daily', 'VK_hook' );

// add_action( 'VK_hook', 'gv2018_vkontakte_users', 10, 0 );

/**
 * Количество пользователей Одноклассники
 */
function gv2018_odnoklassniki_users() {
    $json_daily_file = __DIR__.'/json/odnoklassniki.json';
    if (!is_file($json_daily_file) || filemtime($json_daily_file) < time() - 86400) {
        if ($json_daily = @file_get_contents('https://api.ok.ru/fb.do?application_key=CBADPJLLEBABABABA&counterTypes=members&format=json&group_id=52125761601602&method=group.getCounters&sig=67ffdaae661524802b0461d4e8b05e52&access_token=tkn1ozQkd7g2MNMBMUo95Y9xCUImgzaqdPNplyOxpLzxyefcuSYzz8bvDLxTMHTXt2Wz2')) {
            file_put_contents($json_daily_file, $json_daily);
        }
    }
    return json_decode(file_get_contents($json_daily_file));
}
return;

// function gv2018_odnoklassniki_users() {
// 	$response = wp_remote_get( 'https://api.ok.ru/fb.do?application_key=CBADPJLLEBABABABA&counterTypes=members&format=json&group_id=52125761601602&method=group.getCounters&sig=67ffdaae661524802b0461d4e8b05e52&access_token=tkn1ozQkd7g2MNMBMUo95Y9xCUImgzaqdPNplyOxpLzxyefcuSYzz8bvDLxTMHTXt2Wz2', array( 
// 		'timeout' => 10, 'httpversion' => '1.1' ) 
// 	);
// if ( ! is_wp_error( $response ) && wp_remote_retrieve_response_code( $response ) === 200 ){
// 	$body = wp_remote_retrieve_body( $response );
// 	$data = json_decode( $body );
// 	update_option( 'gv2018_OK_users', $data->counters->members, false ); 
// 	}
// }

// if( !wp_next_scheduled('OK_hook' ) )
// 	wp_schedule_event( time(), 'daily', 'OK_hook' );

// add_action( 'OK_hook', 'gv2018_odnoklassniki_users', 10, 0 );

/**
 * Количество пользователей Инстаграм
 */
// function gv2018_instagram_users() {
//     $json_daily_file = __DIR__.'/json/instagram.json';
//     if (!is_file($json_daily_file) || filemtime($json_daily_file) < time() - 86400) {
//         if ($json_daily = @file_get_contents('https://api.instagram.com/v1/users/2137741869/?access_token=2137741869.2792668.f042a2dbd5ee4e5fb4e43615adf0e5ab')) {
//             file_put_contents($json_daily_file, $json_daily);
//         }
//     }
//     return json_decode(file_get_contents($json_daily_file));
// }
// return;

// function gv2018_instagram_users() {
// 	$response = wp_remote_get( 'https://api.instagram.com/v1/users/2137741869/?access_token=2137741869.2792668.f7e05fd44f114a6ca25e85b9800c45ee', array( 
// 		'timeout' => 10, 'httpversion' => '1.1' ) 
// 	);
// if ( ! is_wp_error( $response ) && wp_remote_retrieve_response_code( $response ) === 200 ){
// 	$body = wp_remote_retrieve_body( $response );
// 	$data = json_decode( $body );
// 	update_option( 'gv2018_IG_users', $data->data->counts->followed_by, false ); 
// 	}
// }

// if( !wp_next_scheduled('IG_hook' ) )
// 	wp_schedule_event( time(), 'daily', 'IG_hook' );

// add_action( 'IG_hook', 'gv2018_instagram_users', 10, 0 );


/**
 * Количество пользователей Твиттера
 */
// function gv2018_twitter_users() {
//     $json_daily_file = __DIR__.'/json/twitter.json';
//     if (!is_file($json_daily_file) || filemtime($json_daily_file) < time() - 86400) {
//         if ($json_daily = @file_get_contents('https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=gorodskievesti')) {
//             file_put_contents($json_daily_file, $json_daily);
//         }
//     }
//     return json_decode(file_get_contents($json_daily_file));
// }
// return;

// function gv2018_twitter_users() {
// 	$response = wp_remote_get( 'https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=gorodskievesti', array( 
// 		'timeout' => 10, 'httpversion' => '1.1' ) 
// 	);
// if ( ! is_wp_error( $response ) && wp_remote_retrieve_response_code( $response ) === 200 ){
// 	$body = wp_remote_retrieve_body( $response );
// 	$data = json_decode( $body );
// 	update_option( 'gv2018_TW_users', $data[0]->followers_count, false ); 
// 	}
// }

// if( !wp_next_scheduled('TW_hook' ) )
// 	wp_schedule_event( time(), 'daily', 'TW_hook' );

// add_action( 'TW_hook', 'gv2018_twitter_users', 10, 0 );


?>