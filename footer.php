<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gv2018
 */

?>

</div>
<!-- #content -->

<div class="breadcrumbs">
	<div class="container-big">
		<div class="breadcrumbs-list hidden-sm" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php
						if(function_exists('bcn_display'))
						{
							bcn_display();
						}?>
		</div>
		<div class="breadcrumbs-pages">
			<?php 
			if(is_single()) { 
				previous_post_link("→ %link"); } 
			else {
				the_posts_pagination( array(
					'mid_size' => 2,
					'prev_text' => __( '←', 'gv2018' ),
					'next_text' => __( '→', 'gv2018' ),
			) ); } ?>
		</div>
	</div>
</div>

<div class="billboard">
	<div class="container-big">
		<?php if ( is_mobile() ) { 
              gv2018_banners( 'bottom-banner-mobile' ); 
              } else {
              gv2018_banners( 'bottom-banner-desktop' ); 
              } ?>
	</div>
</div>

<footer id="colophon" class="site-footer">
	<div class="container-big">
		<div class="site-info">
			<p>© 2009—<?php echo date( 'Y' );?> ООО «<a href="//www.gorodskievesti.ru">Городские вести</a>»</p>
			<p>E-mail:
				<a href="mailto:info@gorodskievesti.ru">info@gorodskievesti.ru</a>
			</p>
			<p>Телефоны:
				<a href="tel:+79827175980">(982) 717-59-80</a>, <a
					href="tel:+79827175904">(982) 717-59-04</a>
			</p>

			<div class="bottom-socials">
				<div class="bottom-social vk">
					<a href="https://vk.com/gorodskievesti" title="Мы во ВКонтакте">
						<i class="fa fa-vk" aria-hidden="true"></i>
					</a>
				</div>
				<div class="bottom-social odnoklassniki">
					<a href="https://ok.ru/gorodskievesti" title="Мы в Одноклассниках">
						<i class="fa fa-odnoklassniki" aria-hidden="true"></i>
					</a>
				</div>
				<div class="bottom-social feed">
					<a href="//www.gorodskievesti.ru/feed/" title="RSS feed">
						<i class="fa fa-rss" aria-hidden="true"></i>
					</a>
				</div>
			</div>

			<div class="legal-info">
				<span class="legal-page">Возрастное ограничение 16+</span>
			</div>

		</div>

		<div class="bottom-pages">
			<h3 class="h3"><a href="//www.gorodskievesti.ru">Сайт</a></h3>
			<nav>
				<ul>
					<li class="bottom-page">
						<a href="/about/">О сайте</a>
					</li>
					<li class="bottom-page">
						<a href="/contacts/">Контактная информация</a>
					</li>
					<li class="bottom-page">
						<a href="/license/">Условия использования материалов</a>
					</li>
					<li class="bottom-page">
						<a href="/privacy-policy/">Политика обработки персональных данных</a>
					</li>
					<li class="bottom-page">
						<a href="/pdn-agreement/">Согласие на обработку персональных данных</a>
					</li>
					<li class="bottom-page">
						<a href="/comments-rules/">Правила комментирования</a>
					</li>
					<li class="bottom-page">
						<a href="/reklama/">Реклама на сайте</a>
					</li>
					<li class="bottom-page">
						<a href="//catalog96.ru/pervouralsk/search/" rel="noopener">Объявления Первоуральска</a>
					</li>
				</ul>
			</nav>
		</div>

		<div class="bottom-pages hidden">
			<h3 class="h3"><a href="//www.gorodskievesti.ru">Газета</a></h3>
			<nav>
				<ul>
					<li class="bottom-page">
						<a href="/about/">О газете</a>
					</li>
					<li class="bottom-page">
						<a href="/contacts/">Контактная информация</a>
					</li>
					<li class="bottom-page">
						<a href="/reklama/">Реклама в газете</a>
					</li>
					<li class="bottom-page">
						<a href="https://bit.ly/2Glnj3c">Точки распространения</a>
					</li>
				</ul>
			</nav>
		</div>

		<div class="bottom-pages">
			<h3 class="h3"><a href="//www.gorodskievesti.ru/info/">Справочная</a></h3>
			<nav>
				<ul>
					<li class="bottom-page">
						<a href="/info/sektsii-kruzhki-studii-v-pervouralske-uchebnyj-god-2019-2020/">Секции, кружки, студии в
							Первоуральске</a>
					</li>
					<li class="bottom-page">
						<a href="/vizitki/">Визитки, флаеры, буклеты, листовки в Первоуральске</a>
					</li>
					<li class="bottom-page">
						<a href="/info/bus/">Расписание автобусов Первоуральска</a>
					</li>
					<li class="bottom-page">
						<a href="/info/medicine/">Больницы и поликлиники Первоуральска</a>
					</li>
					<li class="bottom-page">
						<a href="/info/apteki/">Аптеки Первоуральска</a>
					</li>
					<li class="bottom-page">
						<a href="/info/shtrafy-gibdd/">Проверка штрафов ГИБДД</a>
					</li>
					<li class="bottom-page">
						<a href="/info/proverka-avto-po-vin/">Проверка авто по VIN и гос номеру</a>
					</li>
					<li class="bottom-page">
						<a href="/info/poleznye-telefony/">Полезные телефоны</a>
					</li>
					<li class="bottom-page">
						<a href="/pogoda/">Погода в Первоуральске</a>
					</li>
					<li class="bottom-page">
						<a href="/gostinitsa-pervouralsk-kogda-v-otele-kak-doma/">Отель Первоуральск</a>
					</li>
				</ul>
			</nav>
		</div>



	</div>

		<?php //Начало. Проверка на Режим отладки
			if (WP_DEBUG === false) { 
		?>

	<div class="container-big">
		<div class="site-counters hidden-xs">
			<div class="site-counter">
				<!-- UralWeb counter-->
				<span id="uralweb-hc"></span>
				<script type="text/javascript">
					(function () {
						var hc = document.createElement('script');
						hc.type = 'text/javascript';
						hc.async = true;
						hc.src = '//j.uralweb.ru/js/gorodskievesti.ru';
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(hc, s);
					})();
				</script>
				<!-- end of counter UralWeb -->
			</div>
			<div class="site-counter">
				<!-- Yandex.Metrika informer --> <a href="https://metrika.yandex.ru/stat/?id=193833&amp;from=informer" target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/193833/3_0_515151FF_313131FF_1_pageviews" style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="193833" data-lang="ru" /></a> <!-- /Yandex.Metrika informer --> <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(193833, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/193833" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
			</div>
			<div class="site-counter">
				<img src="//yandex.ru/cycounter?www.gorodskievesti.ru" width="88" height="31" loading="lazy"
					alt="Индекс цитирования Яндекс" style="border-width:0;" />
			</div>
			<div class="site-counter">
				<a rel="license noopener" href="//creativecommons.org/licenses/by-nc-sa/3.0/"><img
						alt="Лицензия Creative Commons" style="border-width:0;" loading="lazy"
						src="//img.gorodskievesti.ru/wp-content/uploads/2014/06/88x31.png" width="88" height="31" /></a>
			</div>
			<div class="site-counter">
				<a href="http://www.trubnik.info/"><img
						src="//img.gorodskievesti.ru/wp-content/uploads/2009/12/ut.gif" loading="lazy" width="88" height="31"
						alt="Уральский трубник (Первоуральск)" style="border-width:0;" /></a>
			</div>
		</div>
	</div>

		<?php //Конец. Проверка на Режим отладки
		} ?>

</footer>
<!-- #colophon -->
</div>
<!-- #page -->

<?php wp_footer(); ?>

</body>
</html>