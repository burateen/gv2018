<?php
/**
 * The template for displaying standart page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gv2018
 */

get_header(); 

gv2018_close_post_button();

?>



	<div class="container">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">

				<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content-page', get_post_type() );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

			</main>
			<?php get_sidebar(); ?>
		</div>
	</div>


	<?php
get_footer();