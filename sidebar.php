<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gv2018
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside class="widget-area stickyColumn">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
	<div class="popular_today">
			<h3>
				Горячие темы
				<span style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/fire.svg);"></span>
			</h3>
				<?php
		if ( function_exists('wpp_get_mostpopular') ) {
					wpp_get_mostpopular(array(
						'limit' => 10,
						'range' => 'last7days',
						'post_type' => 'post',
						'stats_views' => 0,
						'post_html' => '<li><a href="{url}?utm_source=top_sidebar">{text_title}</a></li>'
				));
		}
?>
	</div>
</aside>
